# How to become better programmer
Spoiler: Make extensive use of the command line.

## Write faster in the Terminal
* [Bash shortcuts](https://github.com/fliptheweb/bash-shortcuts-cheat-sheet)

## Vim (done right)
Why should I learn Vim? Seems pretty hard...

[This is why](https://www.youtube.com/watch?v=_NUO4JEtkDw)

### Get Vim up and running

Install:
```
sudo apt-get update
sudo apt-get install vim
```

Setup:
[Install Vim configuration](https://github.com/amix/vimrc)

### Vimtutor
Practice. A lot.
Oh, and get rid of your mouse. Seriously.

```
vimtutor
```

### Stuck? 

![Official cheat sheet](http://www.viemu.com/vi-vim-cheat-sheet.gif "http://www.viemu.com/vi-vim-cheat-sheet.gif")


### Helpful Links

[cheat sheet](https://vim.rtorr.com/)


### I want more!

Try the [chrome plugin](https://vimium.github.io/) and transform your Browser into a mouseless speedy beast
